﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private GameObject AI;

    void Start()
    {
        Instantiate(AI, transform.position, Quaternion.identity); // temp
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
