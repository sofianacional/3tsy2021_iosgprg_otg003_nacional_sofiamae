﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ShootButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    bool isPressed;
    public void Update() // ** alternative with using Update() **
    {
        if (Player.Instance.equipped == null)
            return;
        if (Player.Instance.equipped.CompareTag("Auto"))
        {
            if(isPressed)
            {
                Player.Instance.Shoot();
            }
        }
    }
    public void OnPointerDown(PointerEventData data)
    {
        isPressed = true;
    }
    public void OnPointerUp(PointerEventData data)
    {
        isPressed = false;
    }
    public void OnPointerClick(PointerEventData data)
    {
        Player.Instance.Shoot();
    }

    // Reference : https://answers.unity.com/questions/1544787/how-to-make-the-button-respond-to-touch-and-hold-f.html
}
