﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : Gun
{
    protected override void Start()
    {
        base.Start();
    }
    public override void Fire()
    {
        base.Fire();
        allowFire = false;

        StartCoroutine("FiringRate");

        int bulletCount = 3;
        float spread = 20;

        float angleStep = spread / bulletCount;
        float centeringOffset = (spread / 2) - (angleStep / 2);
        float aimingAngle = firePoint.rotation.eulerAngles.z;

        for (int i = 0; i < bulletCount; i++)
        {
            float currentBulletAngle = angleStep * i;

            Quaternion rotation = Quaternion.Euler(new Vector3(0, 0, aimingAngle + currentBulletAngle - centeringOffset));
            GameObject bulletInstance = Instantiate(bulletObject, firePoint.position, rotation);
        }
        // References : https://answers.unity.com/questions/1775534/how-to-make-a-bullet-spread-shotgun-in-unity-2d.html
        //              https://www.reddit.com/r/Unity2D/comments/gh9rrf/how_can_i_create_a_bullet_spread_in_2d/
    }
}
