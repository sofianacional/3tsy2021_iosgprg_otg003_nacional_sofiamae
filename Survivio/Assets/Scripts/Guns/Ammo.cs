﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour
{
    public int damage;
    public float bulletSpeed;
    public Rigidbody2D bulletRb;

    private void Update()
    {
        bulletMovement();
    }
    public virtual void bulletMovement()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
       if(col.gameObject.CompareTag("Obstacle"))
        {
            Destroy(gameObject);
        }

       if (col.gameObject.GetComponent<Unit>() && 
            col.gameObject.GetComponent<Unit>() != this.gameObject)
        {
            col.gameObject.GetComponent<Unit>().TakeDamage(damage);
        }
    }

    public void TakeDamage()
    {
        GetComponent<Unit>().TakeDamage(damage); // fix
    }
}
