﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunSpawner : MonoBehaviour
{
    [SerializeField] private Transform[] spawnLocations;
    [SerializeField] private GameObject[] items;

    void Start()
    {
        for(int i = 0; i < spawnLocations.Length; i++)
        {
            GameObject spawner = Instantiate(items[Random.Range(0, items.Length)],
            spawnLocations[i].position, Quaternion.identity);

            /*
            AutoItem auto = spawner.GetComponent<AutoItem>();
            auto.Init(autoUI);


            ShotgunItem shotgun = spawner.GetComponent<ShotgunItem>();
            shotgun.Init(shotgunUI);

            PistolItem pistol = spawner.GetComponent<PistolItem>();
            pistol.Init(pistolUI);
            */
        }
        
    }
}
