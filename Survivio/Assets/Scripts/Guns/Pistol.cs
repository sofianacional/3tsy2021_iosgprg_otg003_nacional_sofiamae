﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : Gun
{
    protected override void Start()
    {
        base.Start();
    }
    public override void Fire()
    {
        base.Fire();

        GameObject bulletInstance = (GameObject)Instantiate(bulletObject, firePoint.position,
            firePoint.rotation);
    }
}
