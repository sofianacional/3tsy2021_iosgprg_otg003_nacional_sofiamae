﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public int bullets;
    public int maxMag;
    public int curMag;
    public float fireRate;

    public Sprite Sprite;
    public Ammo ammoType;
    public GameObject bulletObject;
    public Transform firePoint;

    public bool allowFire;
    protected virtual void Start()
    {
        allowFire = true;
    }

    public virtual void Fire()
    {
        
    }

    public virtual IEnumerator FiringRate()
    {
        yield return new WaitForSeconds(fireRate);
        allowFire = true;
    }
}
