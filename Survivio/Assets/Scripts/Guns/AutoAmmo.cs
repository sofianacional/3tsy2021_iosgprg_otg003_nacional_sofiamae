﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoAmmo : Ammo
{
    public override void bulletMovement()
    {
        base.bulletMovement();
        bulletRb.velocity = transform.up * bulletSpeed;

    }
}
