﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponItems : Loot
{
    public GameObject item;

    protected override void Start()
    {
        base.Start();
    }

    public override void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);

        if (col.gameObject.CompareTag("Player"))
        {
            GameObject gunInstance = Instantiate(item,
                Player.Instance.gunHolder.position,
                Player.Instance.gunHolder.rotation);

            col.GetComponent<Player>().AddItem(gunInstance);
            gunInstance.transform.parent = Player.Instance.transform;
        }
    }

}
