﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoItems : Loot
{
    public int amount;
    public GameObject ammoType;
    public bool isPickedUp;

    string ammoTag;
    string equippedTag;

    protected override void Start()
    {
        ammoType = this.gameObject;
    }
    public override void OnTriggerEnter2D(Collider2D col) // Add ammo to inventory
    {
        base.OnTriggerEnter2D(col);

        if (col.CompareTag("Player"))
        {
            // ** ISSUE: Ammo adds up to inventory, Ammo adds to equipped BUT any gun pick up
            //           ammo must be based on ammo picked up before new gun is picked up
            Player.Instance.AddAmmo(ammoType, amount);

            if(this.CompareTag("Pistol"))
            {
                Player.Instance.ammoInventory[2] += amount;
                Debug.Log("added " + amount);
            }
            else if (this.CompareTag("Auto"))
            {
                Player.Instance.ammoInventory[0] += amount;
                Debug.Log("added " + amount);
            }
            else if (this.CompareTag("Shotgun"))
            {
                Player.Instance.ammoInventory[1] += amount;
                Debug.Log("added " + amount);
            }
        }


    }
}
