﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private Transform player;

    // Update is called once per frame
    void Update() 
    {
        transform.position = new Vector3(Mathf.Clamp(player.position.x, -31, 31),
            Mathf.Clamp(player.position.y, -20, 20), transform.position.z);
    }
}
