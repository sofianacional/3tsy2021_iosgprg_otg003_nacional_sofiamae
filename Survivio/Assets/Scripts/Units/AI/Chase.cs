﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chase : AIBaseFSM
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateUpdate(animator, stateInfo, layerIndex);
        var direction = player.transform.position - AI.transform.position;
        AI.transform.rotation = Quaternion.Slerp(AI.transform.rotation, Quaternion.LookRotation(direction),
                                    rotSpeed * Time.deltaTime);
        AI.transform.Translate(0, 0, Time.deltaTime);
    }


}
