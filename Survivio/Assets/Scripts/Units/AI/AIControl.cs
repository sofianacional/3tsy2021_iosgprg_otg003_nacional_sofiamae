﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIControl : Unit
{
    [SerializeField] private Animator anim;
    public GameObject player;
    //public Transform gunHolder;
    public GameObject[] guns;

    Vector3 wanderTarget;
    public float speed;

    //public float offset;
    private GameObject target; // could be Player or another AI
    private Vector3 targetPos;
    private Vector3 thisPos;
    private float angle;
    
    public GameObject GetPlayer()
    {
        return player;
    }
    void Start()
    {
        anim = this.GetComponent<Animator>();

        // Set random equipped for AI
        // guns[Random.Range(0, guns.Length)];
        GameObject gunInstance = Instantiate(guns[1], gunHolder.position, gunHolder.rotation);
        equipped = gunInstance;
        gunInstance.transform.parent = this.transform;

    }

    void Update()
    {

    }

    public override void Shoot()
    {
        base.Shoot();
        equipped.GetComponent<Gun>().Fire();
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Unit>()) // in range of any unit
        {
            target = collision.gameObject;
            anim.SetBool("hasTarget", true);
        }
    }
    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<Unit>())
        {
            anim.SetBool("hasTarget", false);
        }
    }

    // == AI Movement ==
    public void LookAtTarget()
    {
        if(target != null)
        {
            targetPos = target.transform.position;
            //Debug.Log(targetPos);
            thisPos = transform.position;
            //targetPos.x = targetPos.x - thisPos.x;
            //targetPos.y = targetPos.y - thisPos.y;

            angle = Mathf.Atan2(targetPos.y, targetPos.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
            
        }
    }
    public void Wander()
    {
        float wanderRadius = 2;
        float wanderDistance = 5;

        wanderTarget += new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), 0);
        wanderTarget.Normalize();
        wanderTarget *= wanderRadius;

        Vector3 targetLocal = wanderTarget + new Vector3(0, 0, wanderDistance);
        Vector3 targetWorld = transform.InverseTransformVector(targetLocal);

        transform.position = Vector2.MoveTowards(transform.position, targetWorld,
                                GetComponent<AIControl>().speed * Time.deltaTime);
    }

    public IEnumerator Idling()
    {
        yield return new WaitForSeconds(2);
    }

    public void StartShooting()
    {
        InvokeRepeating("Shoot", 0.5f, 0.5f);
    }
    public void StopShooting()
    {
        CancelInvoke("Shoot");
    }
}
