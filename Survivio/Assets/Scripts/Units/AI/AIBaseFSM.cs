﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBaseFSM : StateMachineBehaviour
{
    public GameObject AI;
    public GameObject player; // target

    [SerializeField] protected float rotSpeed;
    [SerializeField] protected float speed;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        AI = animator.gameObject;
        player = AI.GetComponent<AIControl>().GetPlayer();
    }
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
