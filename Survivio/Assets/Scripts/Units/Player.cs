﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class Player : Unit
{
    public static Player Instance { get; private set; }

    public GameObject[] inventory;
    public int[] ammoInventory; // 0 = auto, 1 = shotgun, 2 = pistol
    public Image primaryUI;
    public Image secondaryUI;

    //public TMP_Text autoAmmo;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }
    private void Start()
    {
        Init("Player", 100, 100);
        primaryUI.enabled = false;
        secondaryUI.enabled = false;
    }
    private void Update()
    {
        
    }
    public override void Shoot()
    {
        base.Shoot();
        if (equipped.GetComponent<Gun>().curMag < 1) // no bullets => reload and no shoot
        {
            StartCoroutine("ReloadTimer");
            return;
        }
        
        equipped.GetComponent<Gun>().Fire();
        equipped.GetComponent<Gun>().curMag--;
    }
    public void AddItem(GameObject gun) // pickup of gun
    {
        equipped = gun;
        Debug.Log(equipped);

        if (gun.CompareTag("Pistol"))
        {
            inventory[1] = equipped;
            secondaryUI.enabled = true;
            secondaryUI.sprite = gun.GetComponent<Gun>().Sprite;

            inventory[1].GetComponent<Gun>().bullets += ammoInventory[2];
        }
        else 
        {
            inventory[0] = equipped;
            primaryUI.enabled = true;
            primaryUI.sprite = gun.GetComponent<Gun>().Sprite;

            if (gun.CompareTag("Auto"))
                inventory[0].GetComponent<Gun>().bullets += ammoInventory[0];
            else if(gun.GetComponent("Shotgun"))
                inventory[0].GetComponent<Gun>().bullets += ammoInventory[1];
        }
    }
    public void AddAmmo(GameObject ammoType, int amount) // Add ammo to player equipped
    {
        if (ammoType.CompareTag("Pistol"))
        {
            ammoInventory[2] += amount;
        }
        if (ammoType.CompareTag("Auto")) 
        {
            ammoInventory[0] += amount;
        }
            
        if (ammoType.CompareTag("Shotgun"))
            ammoInventory[1 ] += amount;
    }
    
    public void EquipGun() // and swap gun
    {
        if (inventory == null)
            return;
        if (equipped.CompareTag("Pistol"))
        {
            if (inventory[0] == null)
                return;

            equipped = inventory[0];
        }
        else
        {
            if (inventory[1] == null)
                return;

            equipped = inventory[1];
        }
       
    }
    public void Reload()
    {
        equipped.GetComponent<Gun>().bullets -= equipped.GetComponent<Gun>().maxMag - 
                                                  equipped.GetComponent<Gun>().curMag;
        equipped.GetComponent<Gun>().curMag = equipped.GetComponent<Gun>().maxMag;
        
    }
    IEnumerator ReloadTimer()
    {
        yield return new WaitForSeconds(2);
        Reload();
    }
}
