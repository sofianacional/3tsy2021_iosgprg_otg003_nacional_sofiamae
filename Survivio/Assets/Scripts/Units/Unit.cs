﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    //public string unitName;
    public Transform gunHolder;

    public int maxHP;
    public int curHP;

    public GameObject equipped; // current gun

    public virtual void Init(string nName, int nMaxHP, int nCurHP)
    {
        this.name = nName;
        this.maxHP = nMaxHP;
        this.curHP = nCurHP;
    }

    public virtual void Shoot()
    {
        //equipped.Fire();
    }
    public virtual void TakeDamage(int damage)
    {
        if (gameObject.GetComponent<Unit>().curHP < 0)
        {
            if(gameObject.CompareTag("Player"))
            {
                // Game over
            }
            else
                Destroy(gameObject);
        }

        this.curHP -= damage;
    }
}
