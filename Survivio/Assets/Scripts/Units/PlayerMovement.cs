﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed;
    public JoystickController joystick;
    public Rigidbody2D rb;

    public Vector3 move { get; set; }

    public void FixedUpdate()
    {
        move = ControllerInput();
        Movement();
    }

    private void Movement()
    {
        rb.position = new Vector2(Mathf.Clamp(rb.position.x, (float)-40, (float)40),
                                  Mathf.Clamp(rb.position.y, (float)-24.5, (float)24.5));

        //rb.AddForce((move * speed * Time.deltaTime));
        Player.Instance.transform.position += new Vector3(move.x * speed * Time.deltaTime, move.y * speed * Time.deltaTime, 0);
    }

    private Vector3 ControllerInput()
    {
        Vector3 dir = Vector3.zero;

        dir.x = joystick.Horizontal();
        dir.y = joystick.Vertical();

        if (dir.magnitude > 1)
            dir.Normalize();

        return dir;
    }
}
