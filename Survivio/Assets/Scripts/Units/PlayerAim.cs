﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAim : MonoBehaviour
{
    public float rotSpeed;
    public JoystickController joystick;

    public void FixedUpdate()
    {
        Rotation();
    }

    private void Rotation()
    {
        Vector3 moveAim = (Vector3.up * joystick.Vertical() - Vector3.left * joystick.Horizontal());
        if (joystick.Horizontal() != 0 || joystick.Vertical() != 0)
        {
            transform.rotation = Quaternion.LookRotation(Vector3.forward, moveAim);
        }
        // Reference : 
    }

}
