﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class UIManager : MonoBehaviour
{
    public TMP_Text totalBullets;
    public TMP_Text bulletsInMag;

    AmmoItems ammoItem;
    public TMP_Text autoAmmo;
    public TMP_Text shotgunAmmo;
    public TMP_Text pistolAmmo;

    public Image bar;
    public TMP_Text hp;

    private void Start()
    {
        ammoItem = GetComponent<AmmoItems>();
    }
    private void Update()
    {
        DisplayAmmo();
        AddedAmmo();
        HealthBar();
    }
    void DisplayAmmo()
    {
        if (Player.Instance.equipped == null)
            return;

        totalBullets.text = Player.Instance.equipped.GetComponent<Gun>().bullets.ToString();
        bulletsInMag.text = Player.Instance.equipped.GetComponent<Gun>().curMag.ToString();
    }

    void AddedAmmo()
    {
        autoAmmo.text = Player.Instance.ammoInventory[0].ToString();
        shotgunAmmo.text = Player.Instance.ammoInventory[1].ToString();
        pistolAmmo.text = Player.Instance.ammoInventory[2].ToString();
    }

    void HealthBar()
    {
        bar.fillAmount = Player.Instance.curHP / 100;
        hp.text = Player.Instance.curHP + " / " + Player.Instance.maxHP;
    }
}
