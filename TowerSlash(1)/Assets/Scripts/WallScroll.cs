﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallScroll : MonoBehaviour
{
    public Transform player;
    public GameObject wallPrefab;
    public List<GameObject> activeWalls = new List<GameObject>();
    public float ySpawn;
    public float tileLength;
    public Vector3 wallPos = new Vector3(2.74f, 0, 0);

    void Start()
    {
        spawnWall();
    }

    void Update()
    {
        if(player.position.y > ySpawn - tileLength)
        {
            spawnWall();
            deleteWall();
        }
    }
    void spawnWall()
    {
        GameObject activeWall = Instantiate(wallPrefab, wallPos + (transform.up * ySpawn), Quaternion.identity);
        activeWalls.Add(activeWall);
        ySpawn += tileLength;
    }

    void deleteWall()
    {
        Destroy(activeWalls[0]);
        activeWalls.RemoveAt(0);
    }
}
