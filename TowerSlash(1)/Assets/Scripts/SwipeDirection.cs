﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeDirection : MonoBehaviour
{
    public enum Direction 
    { 
        up = 0, 
        down = 1, 
        left = 2, 
        right = 3
    };
    
    public static Direction determineDirection(Vector2 startPos, Vector2 lastPos)
    {
            if (Mathf.Abs(lastPos.x - startPos.x) > Mathf.Abs(lastPos.y - startPos.y)) // horizontal
            {
                if ((lastPos.x > startPos.x))
                {   //Right swipe
                    return Direction.right;
                }
                else
                {   //Left swipe
                    return Direction.left;
                }
            }
            else // Vertical
            {
                if (lastPos.y > startPos.y)
                {   //Up swipe
                    return Direction.up;
                }
                else
                {   //Down swipe
                    return Direction.down;
                }
            }
        // Reference: https://forum.unity.com/threads/simple-swipe-and-tap-mobile-input.376160/
    }
}
