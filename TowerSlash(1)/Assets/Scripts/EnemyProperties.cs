﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyProperties : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] Transform arrowObj;
    public SwipeDirection.Direction dir; //enemy dir
    PlayerProperties player;
    CompareDirections comp;

    // ARROWS
    public enum ArrowTypes
    {
        green = 0,
        red = 1,
        rotating = 2
    }
    int randType; //rand arrow type
    int randDir;  // rand arrow direction
    bool isRotating;
    ArrowTypes arrowType;
    
    [SerializeField] Sprite[] arrowArray;
    [SerializeField] SpriteRenderer rightArrow;
    void Start()
    {
        player = gameObject.GetComponent<PlayerProperties>();
        randType = Random.Range(0, 3);
        randDir = Random.Range(0, 3);

        StartCoroutine(randomArrows()); // randomizes the type of arrow
        dir = enemyDirection();
    }

    void Update()
    {
        this.transform.Translate(0, -speed * Time.deltaTime, transform.position.z);
    }

    IEnumerator randomArrows()
    {
        if(randType == 0) // arrowTypes.green
        {
            rightArrow.sprite = arrowArray[0];
            arrowType = ArrowTypes.green;
            arrowOrientation(arrowType);
            yield return null;
        }
        else if(randType == 1) // arrowTypes.red
        {
            rightArrow.sprite = arrowArray[1];
            arrowType = ArrowTypes.red;
            arrowOrientation(arrowType);
            yield return null;
        }
        else if(randType == 2) // arrowTypes.rot
        {
            rightArrow.sprite = arrowArray[0];
            isRotating = true;
            while(isRotating)
            {
                arrowObj.Rotate(Vector3.forward, 350 * Time.deltaTime);
                yield return null;
            }
            arrowType = ArrowTypes.rotating;
            arrowOrientation(arrowType);
        }
    }

    void arrowOrientation(ArrowTypes arrowType) //sprites only
    {
        if (arrowType == ArrowTypes.red) // inverted direction of arrows
        {
            if (randDir == 0) // down sprite
                arrowObj.rotation = Quaternion.Euler(0, 0, -90);
            else if (randDir == 1) // up sprite
                arrowObj.rotation = Quaternion.Euler(0, 0, 90);
            else if (randDir == 2) // right sprite
                arrowObj.rotation = Quaternion.Euler(0, 0, 0);
            else if(randDir == 3) // left sprite
                arrowObj.rotation = Quaternion.Euler(0, 0, 180);
        }
        else
        {
            if (randDir == 0) // up
                arrowObj.rotation = Quaternion.Euler(0, 0, 90);
            else if (randDir == 1) // down
                arrowObj.rotation = Quaternion.Euler(0, 0, -90);
            else if (randDir == 2) // left
                arrowObj.rotation = Quaternion.Euler(0, 0, 180);
        }
        
    }

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("InRange"))
            isRotating = false;
        if (col.gameObject.CompareTag("Player"))
        {
            if(col.gameObject.GetComponent<PlayerProperties>().isInvul)
            {
                comp = col.gameObject.GetComponentInChildren<CompareDirections>();
                EnemyProperties currentEnemy = comp.enemies[0];
                comp.enemies.Remove(currentEnemy);
                Destroy(currentEnemy.gameObject);
            }
            else if(col.gameObject.GetComponent<PlayerProperties>().isFrenzy)
            {
                    player.streak = 0;
            }
            else
            {
                SceneManager.LoadScene("RestartGame");
            }
        }
            
    }

    public SwipeDirection.Direction enemyDirection()
    {
        if (randDir == 0)
            return SwipeDirection.Direction.up;
        else if (randDir == 1)
            return SwipeDirection.Direction.down;
        else if (randDir == 2)
            return SwipeDirection.Direction.left;
        else
            return SwipeDirection.Direction.right;
    }
}
