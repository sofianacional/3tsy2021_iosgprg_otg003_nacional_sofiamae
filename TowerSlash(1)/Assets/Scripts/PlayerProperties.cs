﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UI;

public class PlayerProperties : MonoBehaviour
{
    // === PLAYER ===
    [SerializeField] private float speed;
    bool isDashing;
    [SerializeField] private Rigidbody2D rb;
    public SwipeDirection.Direction dir;

    float boostSpd = 4;
    float tempSpd;

    // === SKILLS ===
    [SerializeField] float autoCd;
    float remainCdAuto = 0;

    [SerializeField] float invulCd;
    float remainCdInvul = 0;

    float skillDuration = 2;
    public bool isInvul;

    // ** HARD SKILL **
    [SerializeField] GameObject frenzyButton;
    public Text streakTxt;
    float modeActivateCount;
    public bool isFrenzy;
    public int streak;
    float waveDuration = 15;

    // === INPUTS ===
    [HideInInspector] public Vector2 startPos;
    [HideInInspector] public Vector2 lastPos;

    [HideInInspector] public bool isSwipe;

    CompareDirections comp;
    private Touch touch;
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        comp = GetComponentInChildren<CompareDirections>();
        tempSpd = speed;
        modeActivateCount = 10;
        frenzyButton.gameObject.SetActive(false);
        streakTxt.gameObject.SetActive(false);

        InvokeRepeating("hardWaveCounter", 1, 1);
    }

    private void Update()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        InputMouse();
#elif UNITY_ANDROID || UNITY_IOS
        InputTouch();
#endif
        this.transform.Translate(0, speed * Time.deltaTime, 0);
        Score.distance++;

        if(modeActivateCount == 0)
        {
            frenzyButton.gameObject.SetActive(true);
        }
        streakTxt.text = "Streak : " + streak.ToString();
    }
    void InputMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isSwipe = true;
            startPos = Input.mousePosition;
            lastPos = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            isSwipe = true;
            lastPos = Input.mousePosition;
            // I kept this feature as per part for the base game 
            // requirement however I really wanted to remove this.
            // Uncomment to use dash feature
            //if (startPos == lastPos)
            //    dashing(4, true);
            //else
            dir = SwipeDirection.determineDirection(startPos, lastPos);
        }
        else
            isSwipe = false;
    }
    void InputTouch() // Touch Input
    {
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    //isSwipe = true;
                    startPos = touch.position;
                    lastPos = touch.position;
                    break;

                case TouchPhase.Moved:
                    break;

                case TouchPhase.Ended:
                    isSwipe = true;
                    lastPos = touch.position;
                    // I kept this feature as per part for the base game 
                    //requirement however I really wanted to remove this
                    // uncomment to use dash feature
                    //if (startPos == lastPos)
                    //    dashing(4, true);
                    //else
                    dir = SwipeDirection.determineDirection(startPos, lastPos);
                    break;
            }
        }
        else
            isSwipe = false;
    }
    void dashing(float dashSpeed, bool isDashing) 
    {
        Debug.Log("dashing");
        speed = dashSpeed;
        StartCoroutine("resetSpd");
    }
    public void autoSkill()
    {
        if(Time.time > remainCdAuto)
        {
            Debug.Log(comp);
            comp.Compare(comp.enemies[0].dir, true);

            remainCdAuto = Time.time + autoCd;
        }
    }
    public void invulnerabilitySkill()
    {
        if (Time.time > remainCdAuto)
        {
            Debug.Log("invulnerable");
            this.speed += boostSpd;
            isInvul = true;
            remainCdInvul = Time.time + invulCd;

            StartCoroutine("resetSpd");
        }
    }
    IEnumerator resetSpd()
    {
        yield return new WaitForSeconds(skillDuration);
        this.speed = tempSpd;
        isInvul = false;
    }
    public void frenzySkill()
    {
        streakTxt.gameObject.SetActive(true);
        isFrenzy = true;
        isInvul = true;

        StartCoroutine("frenzyDuration");
    }
    IEnumerator frenzyDuration()
    {
        yield return new WaitForSeconds(waveDuration);
        Score.distance *= streak;
        streak = 0;
        isFrenzy = false;
        isInvul = false;

        frenzyButton.gameObject.SetActive(false);
        streakTxt.gameObject.SetActive(false);
        modeActivateCount = 10;
    }
    void hardWaveCounter()
    {
        if(modeActivateCount > 0)
            modeActivateCount--;
    }
}
