﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Text displayScore;
    void Update()
    {
        displayScore.text = "Distance: " + Score.distance.ToString();
    }
    public void startGame()
    {
        SceneManager.LoadScene("BeginGame");
        Score.distance = 0;
    }
    
}
