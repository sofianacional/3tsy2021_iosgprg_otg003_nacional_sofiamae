﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private GameObject enemy;

    private Vector3 distance = new Vector3(0, 10, 0);
    void Start()
    {
        StartCoroutine("spawnTimer");
    }

    void Update()
    {
        transform.position = new Vector3(player.transform.position.x + distance.x,
                                        player.transform.position.y + distance.y,
                                        transform.position.z);
    }

    void spawn()
    {
        GameObject spawner = Instantiate(enemy, this.transform.position, Quaternion.identity);
    }

    IEnumerator spawnTimer()
    {
        while(true)
        {
            yield return new WaitForSeconds(Random.Range(1, 3));
            spawn();
        }
    }
}
