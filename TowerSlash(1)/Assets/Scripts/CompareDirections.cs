﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CompareDirections : MonoBehaviour
{
    PlayerProperties player;
    public List<EnemyProperties> enemies = new List<EnemyProperties>(); // List of Enemies in Range
    void Start()
    {
        player = transform.parent.GetComponent<PlayerProperties>();
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.CompareTag("Enemy"))
        {
            enemies.Add(col.GetComponent<EnemyProperties>());
        }
    }

    private void Update()
    {
        Compare(player.dir, player.isSwipe);
    }
    public void Compare(SwipeDirection.Direction playerDir, bool isSwipe)
    {
        if(isSwipe)
        {
            if (enemies.Count > 0) // as an enemy enters within the range of player
            {
                if (playerDir == enemies[0].dir) // Direction checker within enemies in Range; always enemy in [0]
                {
                    if(player.isFrenzy)
                    {
                        player.streak++;
                    }
                    EnemyProperties currentEnemy = enemies[0];
                    enemies.Remove(currentEnemy);
                    Destroy(currentEnemy.gameObject);
                }
                else
                {
                    if(player.isFrenzy)
                    {
                        player.streak = 0;
                    }
                    else
                    {
                        SceneManager.LoadScene("RestartGame");
                    }
                }
            }
        }
        
        
    }
}
